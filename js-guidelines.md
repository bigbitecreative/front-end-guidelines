# Code guidelines: JavaScript

## Indentation
Indentation should always be two spaces. Two spaces minimises horizontal whitespace and sets up a standard for the development team to adhere to.

## General JavaScript Principles
* Use single quotes.
* Use camelCase for naming conventions (expetions are where defining class names from HTML markup)
* Name variables and functions logically: For example, popUpWindowForAd rather than myWindow
* 99% of code should be housed in external JavaScript files. They should be included right before the closing body tag for maximum page performance
* Always use curly braces, even in situations where they are technically optional. Having them increases readability and decreases the likelihood of logic errors being introduced as the codebase continues to evolve
* Opening curly braces should never be on their own new line
* Closing curly braces should always be on their own new line

## JavaScript Libraries
Our library of choice is [jQuery](http://jquery.com/). That said, we promote using as much raw JavaScript where possible.

## Vaidation
Make use of tools like JSLint to make sure code is consistant.
