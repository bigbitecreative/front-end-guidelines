# Code guidelines: HTML


## Indentation
Indentation should always be two spaces. Two spaces minimises horizontal whitespace and sets up a standard for the development team to adhere to.

## General guidelines

* Use the HTML5 Doctype by default.
* Nested elements should be indented once (two spaces).
* Always use double quotes, never single quotes.
* Multiple classes separated by a double space.
* Don't include a trailing slash in self-closing elements.
* All markup should be delivered as UTF-8.
* Use semantic markup.
* Utilise the new HTML5 elements such as `header`, `nav`, `section`, `article`, `aside`, `footer`. Ensure you are using [html5shiv](https://code.google.com/p/html5shiv/).
* Use a hierarchical heading structure to help screen reader users navigate a page.
* For buttons use a `<button>`.
* Use `label` fields to label each form field, the `for` attribute should associate itself with the `id` of the input field, so users can click the labels.
* Tables are not be used for page layout.
* Do not use all caps or all lowercase titles in markup, instead apply with CSS.

**Incorrect example:**


	<!DOCTYPE html>
	<html>
	<head>
	<title>Page title</title>
	</head>
	<body>
	<img src='images/company-logo.png' alt='Company' />
	<h1 class='class_one classTwo'>Hello, world!</h1>
	</body>
	</html>


**Correct example:**

```
<!DOCTYPE html>
<html>
  <head>
    <title>Page title</title>
    <meta charset="utf-8">
  </head>
  <body>
    <img src="images/company-logo.png" alt="Company">
    <h1 class="class-one  class-two">Hello, world!</h1>
  </body>
</html>
```

### Attribute order

HTML attributes should come in this particular order for easier reading of code.

* class
* id
* data-*
* for|type|href

Such that your markup looks like:

```
<a class="" id="" data-modal="" href="">Example link</a>
```


### WAI-ARIA

* Use WAI-ARIA landmark roles to help screen reader users understand and navigate a page.
* Use WAI-ARIA form attributes to help screen reader users to use forms - e.g `aria-required="true"`.
* Use live regions to inform screen reader users of dynamic text changes - e.g `<div aria-live="polite" aria-atomic="true">`
* Follow the [WAI-ARIA 1.0 Authoring Practices](http://www.w3.org/TR/wai-aria-practices/) when implement javascript widgets such as sliders, tooltips and tab panels.

	
### Validation 
Markup should be tested against the W3C validator to ensure that the markup is well formed. 100% valid code is not essential, but validation helps identify possible issues when building maintainable, accessible, robust sites as well as debugging code.