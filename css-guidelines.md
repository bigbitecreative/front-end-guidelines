# Code guidelines: CSS

## Indentation
Indentation should always be two spaces. Two spaces minimises horizontal whitespace and sets up a standard for the development team to adhere to.


## General CSS guidelines

* Use `border-box` content model.
* When grouping selectors, keep individual selectors to a single line.
* Include one space before the opening brace of declaration blocks.
* Place closing braces of declaration blocks on a new line.
* Include one space after `:` in each property.
* Each declaration should appear on its own line.
* End all declarations with a semi-colon.
* Comma-separated values should include a space after each comma.
* Lowercase all hex values, e.g., `#fff` instead of `#FFF`.
* Use shorthand hex values where available, e.g., `#fff` instead of `#ffffff`.
* Quote attribute values in selectors, e.g., `input[type="text"]`.
* Avoid specifying units for zero values, e.g., `margin: 0;` instead of `margin: 0px;`.
* Use a unit-less `line-height` value.
* For calculated length units like em, include a comment with the px equivalent (vanilla CSS only).
* Avoid using IDs (keeps specificity low and promotes reusability).
* Any style you apply to :hover also apply to :focus so that keyboard users get the same visual cues (?).
* Do not make your styles too specific - specifity wars are a maintanence nightmare!
* Use Normalise rather than reset.

**Incorrect example:**


```
.selector, .selector-secondary, .selector[type=text]{
  padding:15px;
  margin:0px 0px 15px;
  background-color:rgba(0, 0, 0, 0.5);
  box-shadow:0 1px 2px #CCC,inset 0 1px 0 #FFFFFF
}
```

**Correct example:**

```
.selector,
.selector-secondary,
.selector[type="text"] {
  padding: 15px;
  margin: 0 0 15px;
  background-color: rgba(0,0,0,.5);
  box-shadow: 0 1px 2px #ccc, inset 0 1px 0 #fff;
}
```

## Prefixed properties

When using vendor prefixed properties, indent each property such that the value lines up vertically for easy multi-line editing.

```
.selector {
  -webkit-border-radius: 3px;
     -moz-border-radius: 3px;
          border-radius: 3px;
}
```
In Textmate, use **Text &rarr; Edit Each Line in Selection** (&#8963;&#8984;A). In Sublime Text 2, use **Selection &rarr; Add Previous Line** (&#8963;&#8679;&uarr;) and **Selection &rarr;  Add Next Line** (&#8963;&#8679;&darr;).


## Class names
* Keep classes lowercase and use dashes (exept in BEM syntax).
* Avoid arbitrary shorthand notation.
* Keep classes as short and succinct as possible.
* Use dashes to separate words.
* Do not use presentational class names such as 'green'.
* Prefix classes based on the closest parent component's base class.
* Instead of using content semantics for class names (e.g news) use intention and design patterns (e.g promo and carousel) .to ensure reusability of the object.
* Minimise the use of element selectors: [http://smacss.com/book/type-module](http://smacss.com/book/type-module).

**Incorrect example:**

```
.tp { ... }
.hd { ... }
.red { ... }
```

**Correct example:**

```
.top { ... }
.head { ... }
.important { ... }
```

## Selectors
* Use classes over generic element tags
* Keep them short and limit the number of elements in each selector to three
* Scope classes to the closest parent when necessary (e.g., when not using prefixed classes)

**Incorrect example:**

```
nav li { ... }
.hero h1 { ... }
```

**Correct example:**

```
.nav__item { ... }
.here__title { ... }
```

## Comments

### Main section
```
/*--------------------------------------*\
    Sidebar
/*--------------------------------------*/
```

### Sub section
```
/**
 * Sub section
 */
```

### Within sub-section
```
/* Item */
```

Or if using Sass, feel free to use:

```
// Item
```




## BEM Naming Convention

Follow the BEM (block, element, modifier) naming convention for assigning clear, conscise and semantic classes to our HTML elements.

An explanation of BEM can be found on [CSS Wizardry](http://csswizardry.com/2013/01/mindbemding-getting-your-head-round-bem-syntax/) but in short following this pattern:

```
.block{}
.block__element{}
.block--modifier{}
```

.block represents the higher level of an abstraction or component.
.block__element represents a descendent of .block that helps form the .block object.
.block--modifier represents a different state or version of .block.

A practical example of this is:

```
.site-search{} /* Block */
.site-search__field{} /* Element */
.site-search--full{} /* Modifier */
```

As noted in the CSS Wizardry article HTML elements marked up with the BEM naming convention can appear "ugly" and verbose. However, where they excell is in their readbility and their contribution to maintainable code.

Please note that the BEM naming style does not need to be used for elements that have no relationship to parent elements or sit on their own. Remember that BEM is used to clarify code, not to be adhered to without question. Avoid over-using BEM syntax causing verbose classes.

**Incorrect exampple:**

```
<header class="page-head">
  <nav clas="page-head__nav">
  	<ul>
  	  <li class="page-head__nav__item">Item</li>
  	</ul>
  </nav>
</header>
```

**Correct example:**

```
<header class="page-head">
  <nav clas="nav">
  	<ul>
  	  <li class="nav__item">Item</li>
  	</ul>
  </nav>
</header>
```
	
Keep everying modular. `nav` only happens to be inside the `page-head`, it may not always live inside it, therefore keep the classes as re-usable as possible. `nav__item` will always live inside the parent `nav`, so it makes sense to use BEM methodology here.


## Object Orientated CSS

In order to create a scaleable, reusable, and maintainable codebase we have adopted object oriented CSS (OOCSS). OOCSS in its simplest terms reduces CSS to generic objects that can be extracted from one location and reused across a website or application without unnecessary repetition.

"single responsibility principle" - everything should do one thing, one thing only, and one thing well. This allows for a greater combination of...

Utilise [Object Oriented CSS](http://coding.smashingmagazine.com/2011/12/12/an-introduction-to-object-oriented-css-oocss/) (OOCSS) and [Scalable and Module Architecture for CSS](http://smacss.com/) (SMACSS)
